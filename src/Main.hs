{-# LANGUAGE OverloadedStrings   #-}
{-# LANGUAGE RecordWildCards     #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import qualified Control.Applicative
import qualified Control.Concurrent                   as Concurrent
import qualified Control.Exception                    as Exception
import qualified Control.Monad                        as Monad
import           Control.Monad                        (void)
import qualified Data.List                            as List
import qualified Data.Maybe                           as Maybe
import           Data.Text
import qualified Data.Text                            as Text
import qualified Data.Text.Encoding                   as Text.Encoding
import qualified Data.Text.Lazy                       as TL
import           Data.Void                            (Void)
import           Debug.Trace                          (trace)
import qualified Network.HTTP.Types                   as Http
import qualified Network.Wai                          as Wai
import qualified Network.Wai.Handler.WebSockets       as WS
import qualified Network.Wai.Middleware.RequestLogger as RequestLogger
import qualified Network.Wai.Middleware.Static        as Static
import qualified Network.WebSockets                   as WS
import           Text.Blaze.Html.Renderer.Text        (renderHtml)
import           Text.Blaze.Html5                     ((!))
import qualified Text.Blaze.Html5                     as H
import qualified Text.Blaze.Html5.Attributes          as A
import           Text.Regex.PCRE
import           Text.Megaparsec                      hiding (State)
import           Text.Megaparsec.Char
import qualified Text.Megaparsec.Char.Lexer           as L
import           Web.Spock                            as Spock
import           Web.Spock.Config                     as Spock
import           Web.Spock.Core                       (SpockCtxT)

data TwcaSession = EmptySession
type TwcaAppState = ()
type Parser = Parsec Void Text

main :: IO ()
main = do
  state <- Concurrent.newMVar []
  spockConfig <- defaultSpockCfg EmptySession PCNoDatabase ()
  runSpock 5000 (spock spockConfig (appMiddlewares >> app))

appMiddlewares :: Web.Spock.Core.SpockCtxT () (WebStateM () TwcaSession TwcaAppState) ()
appMiddlewares = do
  middleware RequestLogger.logStdoutDev
  middleware (Static.staticPolicy $ Static.noDots Static.>-> Static.addBase "static")
  middleware wsMiddleware

wsMiddleware :: Wai.Middleware
wsMiddleware = WS.websocketsOr WS.defaultConnectionOptions wsApp
  where
    wsApp :: WS.ServerApp
    wsApp pendingConn = do
      conn <- WS.acceptRequest pendingConn
      WS.withPingThread conn 30 (return ()) $ do
      WS.sendTextData conn ("..." :: Text)
      counter conn 1
    counter :: WS.Connection -> Int -> IO ()
    counter conn i = do
      Concurrent.threadDelay 50000   -- 50ms
      WS.sendTextData conn (Text.pack $ show i)
      counter conn (i + 1)

app :: SpockM () TwcaSession TwcaAppState ()
app = do
  get root rootAction
  post "ingest" $ body >>= \b ->
    let
      checker user pass =
        Monad.unless (user == "test" && pass == "test") $ do
          setStatus Http.status401 >> text "Unauthorised"

      maybeLog =
        runParser pLog "logs" (Text.Encoding.decodeUtf8 b)

    in requireBasicAuth "Ingest" checker $ \() -> do
      setStatus Http.status200 >> text (Text.pack (show maybeLog))

rootContent :: H.Html
rootContent =
  H.docTypeHtml ! A.lang "en" $ do
    H.head $ do
      H.meta ! A.charset "utf-8"
      H.title "Spock Websockets"
      H.script ! A.type_ "text/javascript" ! A.src "ws.js" $ mempty
      H.link ! A.type_ "text/css" ! A.rel "stylesheet" ! A.href "ws.css"
    H.body $ do
      ""

rootAction :: ActionCtxT ctx (WebStateM () TwcaSession TwcaAppState) a
rootAction = (Spock.html . TL.toStrict . renderHtml) rootContent

data Log = Log
  { size :: Int
  , time :: Text
  , message :: Text
  } deriving (Eq, Show)

pLog :: Parser Log
pLog = do
  size <- L.decimal
  void (char ' ')
  void (char '<')
  void (some alphaNumChar)
  void (char '>')
  void alphaNumChar
  void (char ' ')
  time <- Text.pack <$> some alphaNumChar
  void (char ' ')
  message <- Text.pack <$> some alphaNumChar
  return Log {..}
